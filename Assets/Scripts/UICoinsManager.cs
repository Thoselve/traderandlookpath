using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICoinsManager : MonoBehaviour
{
    private Text textCoinsValue;
    private void Awake()
    {
        textCoinsValue= GetComponent<Text>(); 
    }
    private void Start()
    {
        LootManager.Instance.updateCoinsvEvent += UpdateCountCoins;
    }
    private void OnDisable() 
    {
        LootManager.Instance.updateCoinsvEvent -= UpdateCountCoins;
    }
    private void UpdateCountCoins(int count)
    {
        textCoinsValue.text = count.ToString();
    }
}
