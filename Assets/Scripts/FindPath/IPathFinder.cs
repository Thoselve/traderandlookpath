using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathFinder
{
   public IEnumerable<Vector2> GetPath(Vector2 A, Vector2 C, IEnumerable<Edge> edges);
}
[Serializable]
public struct Rectangle
{
    public Vector2 Min;
    public Vector2 Max;
}
[Serializable]
public struct Edge
{
    public Rectangle First;
    public Rectangle Second;
    public Vector3 Start;
    public Vector3 End;
}
