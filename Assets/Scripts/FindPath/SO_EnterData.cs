using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FindPathData", menuName = "FindPath/Create FindPathdata")]

public class SO_EnterData : ScriptableObject
{
    public List<Edge> edges;
    public Vector2 AStart;
    public Vector2 CEnd;
}
