using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class Node
{
    public Vector2 NodePosition;
    public Vector2 TargetPosition;
    public Node PreviousNode;
    public int CostTotal;
    public int CostFromStart;
    public int CostToTarget;
    public int CostRotate;
    public Vector2 direction;
    public Node(Vector2 nodePosition, Vector2 targetPosition, Node previousNode, int costFromStart)
    {
        NodePosition = nodePosition;
        TargetPosition = targetPosition;
        PreviousNode = previousNode;
        CostFromStart = costFromStart;

        CostToTarget = (int)Mathf.Abs(TargetPosition.x - NodePosition.x) + (int)Mathf.Abs(TargetPosition.y - NodePosition.y);
        if (previousNode != null)
        {
            direction = (PreviousNode.NodePosition - NodePosition);
            if (direction != PreviousNode.direction)
            {
                CostTotal += 5;
            }
        }
        CostTotal = CostFromStart + CostToTarget;
    }
}
