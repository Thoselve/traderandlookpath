using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FindPath : MonoBehaviour, IPathFinder
{
    public IEnumerable<Vector2> PathToTarget = new List<Vector2>();
    List<Node> CheckedNodes = new List<Node>();
    List<Node> WaitingNodes = new List<Node>();
    public SO_EnterData data;

    public short emergencyExit = 0;

    private void Awake()
    {
        PathToTarget = GetPath(data.AStart, data.CEnd, this.data.edges);
        //PrintPath(PathToTarget);
    }

    public IEnumerable<Vector2> GetPath(Vector2 A, Vector2 C, IEnumerable<Edge> edges)
    {
        A = new Vector2(Mathf.Round(A.x), Mathf.Round(A.y));
        C = new Vector2(Mathf.Round(C.x), Mathf.Round(C.y));

        if (!PointIsPartRectangle(A, edges.FirstOrDefault().First))
        {
            return new List<Vector2>();
        }
        if (!PointIsPartRectangle(C, edges.FirstOrDefault().Second))
        {
            return new List<Vector2>();
        }


        Vector2 startPosition = new Vector2(Mathf.Round(A.x), Mathf.Round(A.y));
        Vector2 targetPosition = CalculatePointBehindEdge(edges.FirstOrDefault(), A); //new Vector2(Mathf.Round(C.x), Mathf.Round(C.y));
        if (targetPosition == new Vector2(999, 999))
        {
            Debug.LogError("Error Calculate Points Edge");
            return new List<Vector2>();
        }
        //  Vector2 pointBehindEdge = CalculatePointBehindEdge(edges.First(), A);

        if (startPosition == targetPosition)
        {
            Debug.LogError("Start and End in one point");
            return new List<Vector2>();
        }

        Node startNode = new Node(startPosition, targetPosition, null, 0);
        CheckedNodes.Add(startNode);
        WaitingNodes.AddRange(GetNeibhourNodes(startNode));

        while (WaitingNodes.Count > 0)
        {
            Node nodeToCheck = WaitingNodes.Where(x => x.CostTotal == WaitingNodes.Min(y => y.CostTotal)).FirstOrDefault();

            if (nodeToCheck.NodePosition == targetPosition)
            {
                if (targetPosition != new Vector2(Mathf.Round(C.x), Mathf.Round(C.y)))
                {
                    targetPosition = new Vector2(Mathf.Round(C.x), Mathf.Round(C.y));
                    nodeToCheck.TargetPosition = targetPosition;
                    nodeToCheck.CostFromStart = 0;
                    WaitingNodes.Clear();
                }
                else
                {
                    return CalculatePreviousPathFromNode(nodeToCheck);
                }


            }

            WaitingNodes.Remove(nodeToCheck);
            if (!CheckedNodes.Where((x => x.NodePosition == nodeToCheck.NodePosition)).Any())
            {
                CheckedNodes.Add(nodeToCheck);
                WaitingNodes.AddRange(GetNeibhourNodes(nodeToCheck));
            }


            if (emergencyExit > 30000)
            {
                Debug.Log("Counter emergencyExit greater than 30 000 and Calculate Path break");
                return new List<Vector2>();
            }
            emergencyExit++;
        }



        return PathToTarget;
    }

    public Vector2 CalculatePointBehindEdge(Edge edge, Vector3 A)
    {
        if (edge.Start.z != 0 || edge.End.z != 0)
        {
            Debug.LogError("Start and End edge point position Z can not be 0");
            return new Vector2(999, 999);

        }
        if (edge.Start.x == edge.End.x || edge.Start.y == edge.End.y)
        {
            if (!PointIsPartRectangle(edge.Start, edge.First))
            {
                return new Vector2(999, 999);
            }
            if (!PointIsPartRectangle(edge.End, edge.First))
            {
                return new Vector2(999, 999);
            }
        }
        else
        {
            Debug.LogError("Start or End point not in the same edge");
            return new Vector2(999, 999);

        }

        Vector2 midleEdgePoint = (new Vector2(edge.Start.x, edge.Start.y) + new Vector2(edge.End.x, edge.End.y)) / 2f;
        Vector2 AeNorm = new Vector2(midleEdgePoint.x - A.x, midleEdgePoint.y - A.y).normalized;
        Vector2 pointBehind = new Vector2(midleEdgePoint.x + AeNorm.x, midleEdgePoint.y + AeNorm.y);

        return new Vector2(Mathf.Round(pointBehind.x), Mathf.Round(pointBehind.y));
    }

    public bool PointIsPartRectangle(Vector3 point, Rectangle rectangle)
    {
        if (rectangle.Min.x < rectangle.Max.x)
        {
            if (point.x < rectangle.Min.x || point.x > rectangle.Max.x)
            {
                Debug.LogError("Point not part of a rectangle");
                return false;
            }
        }
        if (rectangle.Min.y < rectangle.Max.y)
        {
            if (point.y < rectangle.Min.y || point.y > rectangle.Max.y)
            {
                Debug.LogError("Point not part of a rectangle");
                return false;
            }
        }

        return true;
    }

    public List<Vector2> CalculatePreviousPathFromNode(Node node)
    {
        List<Vector2> result = new List<Vector2>();
        Node currentNode = node;

        while (currentNode.PreviousNode != null)
        {
            result.Add(new Vector2(currentNode.NodePosition.x, currentNode.NodePosition.y));
            currentNode = currentNode.PreviousNode;
        }

        return result;
    }

    public void OnDrawGizmos()
    {
        if (PathToTarget == null) return;
        if (data == null) return;

        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(data.AStart, 0.3f);
        Gizmos.DrawSphere(data.CEnd, 0.3f);

        foreach (var data in data.edges)
        {

            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector2(data.First.Min.x, data.First.Min.y), new Vector2(data.First.Min.x, data.First.Max.y));
            Gizmos.DrawLine(new Vector2(data.First.Min.x, data.First.Min.y), new Vector2(data.First.Max.x, data.First.Min.y));
            Gizmos.DrawLine(new Vector2(data.First.Max.x, data.First.Max.y), new Vector2(data.First.Min.x, data.First.Max.y));
            Gizmos.DrawLine(new Vector2(data.First.Max.x, data.First.Max.y), new Vector2(data.First.Max.x, data.First.Min.y));

            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector2(data.Second.Min.x, data.Second.Min.y), new Vector2(data.Second.Min.x, data.Second.Max.y));
            Gizmos.DrawLine(new Vector2(data.Second.Min.x, data.Second.Min.y), new Vector2(data.Second.Max.x, data.Second.Min.y));
            Gizmos.DrawLine(new Vector2(data.Second.Max.x, data.Second.Max.y), new Vector2(data.Second.Min.x, data.Second.Max.y));
            Gizmos.DrawLine(new Vector2(data.Second.Max.x, data.Second.Max.y), new Vector2(data.Second.Max.x, data.Second.Min.y));

            Gizmos.color = Color.green;
            Gizmos.DrawLine(data.Start, data.End);


            foreach (Vector2 point in PathToTarget)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(new Vector2(point.x, point.y), 0.2f);
            }


        }
    }

    public List<Node> GetNeibhourNodes(Node node)
    {
        List<Node> neighbours = new List<Node>();

        neighbours.Add(new Node(new Vector2(node.NodePosition.x + 1, node.NodePosition.y), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x - 1, node.NodePosition.y), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x, node.NodePosition.y + 1), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x, node.NodePosition.y - 1), node.TargetPosition, node, node.CostFromStart + 1));

        neighbours.Add(new Node(new Vector2(node.NodePosition.x + 1, node.NodePosition.y + 1), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x - 1, node.NodePosition.y + 1), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x + 1, node.NodePosition.y - 1), node.TargetPosition, node, node.CostFromStart + 1));
        neighbours.Add(new Node(new Vector2(node.NodePosition.x - 1, node.NodePosition.y - 1), node.TargetPosition, node, node.CostFromStart + 1));

        return neighbours;
    }

    private void PrintPath(IEnumerable<Vector2> path)
    {
        Debug.Log("Path have " + path.Count() + " points");
        foreach (var e in path)
        {
            Debug.Log(e);
        }
    }
}
