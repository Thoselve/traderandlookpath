using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StartLoot", menuName = "Item/Create StartPlayerLoot")]
public class SO_StartLoot : ScriptableObject
{
    public List<Item> items;
}
