using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootManager : MonoBehaviour
{

    public delegate void UpdateCoinsCount(int coinsCount);
    public event UpdateCoinsCount updateCoinsvEvent;

    public static LootManager Instance;

    private List<Item> playerItems;
    private List<Item> traderItems;
    public SO_StartLoot StartPlayerLoot;
    public SO_StartLoot StartTraderRoot;

    
    private int coins = 200;
    public int GetCoins { get { return coins; } private set { } }

    public int percentTraderTax = 20;

    public Transform ContentPlayer;
    public Transform ContentTrader;
    public GameObject ItemGameObject;
    public void SetCoins(int amount, ItemOwner newOwner)
    {
        if (newOwner==ItemOwner.Trader)
        {
            coins += amount;
            updateCoinsvEvent?.Invoke(coins);

        }
        else 
        {

            coins -= amount;
            updateCoinsvEvent?.Invoke(coins);
        }
       
    }
   
    public void InitLoot(SO_StartLoot so_StartLoot, Transform contentTransform, List<Item> listItems, ItemOwner owner)
    {
        listItems = so_StartLoot?.items;
        foreach (Item item in listItems)
        {
            GameObject tempObj = Instantiate(ItemGameObject, contentTransform);
            ItemManager tempManager = tempObj.GetComponent<ItemManager>();

            tempManager.InitData(item.name, item.icon, item.price, item.id, owner);
            
        }
    }
    private void Awake()
    {
        Instance = this;
        playerItems = new List<Item>();
        traderItems = new List<Item>();
        InitLoot(StartPlayerLoot,ContentPlayer, playerItems,ItemOwner.Player);
        InitLoot(StartTraderRoot,ContentTrader, traderItems, ItemOwner.Trader);
    }
    public enum ItemOwner
    {
        Player,
        Trader
    }
}
