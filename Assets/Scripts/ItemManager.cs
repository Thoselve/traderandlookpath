using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEditor.Progress;
using static UnityEngine.UI.GridLayoutGroup;

public class ItemManager : MonoBehaviour,IDragHandler,IBeginDragHandler,IEndDragHandler
{
    private RectTransform rectTransform;
    private Transform parentCanvas;
    private Transform oldParentContent;
    private GraphicRaycaster raycaster;
    private List<RaycastResult> raycastResult;

    public Text itemPriceText { get; set; }
    public Text itemText { get; set; }
    public Image itemImage { get; set; }
    public int Price { get; set; }
    public int ID { get; set; }
    public LootManager.ItemOwner Owner { get; set; }
    
    private int defaultPrice;

    private void Awake()
    {
        
        rectTransform = GetComponent<RectTransform>();
       
        parentCanvas = transform.root.GetComponent<Transform>();
        oldParentContent = transform.parent;
        raycaster=transform.root.GetComponent<GraphicRaycaster>();
        raycastResult = new List<RaycastResult>();
        
        itemPriceText= transform.Find("Price").GetComponent<Text>();
        itemText= transform.Find("Text").GetComponent<Text>();
        itemImage= transform.Find("Image").GetComponent<Image>();
    }
    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.position = Input.mousePosition;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetParent(parentCanvas);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //if parent not change
        transform.SetParent(oldParentContent);

        raycastResult.Clear();
        raycaster.Raycast(eventData, raycastResult);
        if(raycastResult.Count > 0 )
        {
            foreach(RaycastResult r in raycastResult)
            {
                if (r.gameObject.layer == 6)
                {
                    //transform.SetParent(r.gameObject.transform.GetChild(0));
                    if (r.gameObject.tag == "PlayerContent")
                    {
                        if (Owner == LootManager.ItemOwner.Player)
                        {
                            break;
                        }
                        else if (Owner == LootManager.ItemOwner.Trader)
                        {
                            if (CanTrade())
                            {
                                Owner = LootManager.ItemOwner.Player;
                                LootManager.Instance.SetCoins(Price, LootManager.ItemOwner.Player);
                                ChangePrice(true);
                                transform.SetParent(r.gameObject.transform.GetChild(0));
                                oldParentContent = r.gameObject.transform.GetChild(0);
                            }
                            else
                            {
                                
                               transform.SetParent(oldParentContent);
                            }

                        }
                        
                    }
                    else if (r.gameObject.tag == "TraderContent")
                    {
                        if (Owner == LootManager.ItemOwner.Trader)
                        {
                            break;
                        }
                        else if (Owner == LootManager.ItemOwner.Player)
                        {
                            Owner = LootManager.ItemOwner.Trader;
                            LootManager.Instance.SetCoins(Price, LootManager.ItemOwner.Trader);
                            ChangePrice(false);
                            transform.SetParent(r.gameObject.transform.GetChild(0));
                            oldParentContent = r.gameObject.transform.GetChild(0);
                        }
                        
                    }
                  
                }
            }
        }    
        
      
    }
    public void InitData(string name, Sprite icon, int price, int id, LootManager.ItemOwner owner)
    {
        itemText.text = name;
        itemImage.sprite = icon;
        itemPriceText.text = price.ToString();
        Price = price;
        defaultPrice = price;
        ID = id;
        Owner = owner;
        //price items cheaper in player inventory 
        if(Owner==LootManager.ItemOwner.Player)
        {
            ChangePrice(true);
        }
    }
    public void ChangePrice(bool withTax)
    {
        Price = defaultPrice;
        if(withTax==true)
        {
            double tax = (double)defaultPrice;
            tax = (tax / 100) * (100 - LootManager.Instance.percentTraderTax);
            Price = (int)tax;
        }
        
        itemPriceText.text = Price.ToString();
    }
    
    public bool CanTrade()
    {
        if(Price<=LootManager.Instance.GetCoins)
        {
            return true;
        }
        else return false;
    }
}
